sumanthmaddirala_interviewcode
NexusEdge

1) password_generator.py
This script serves the two main functions responsible for generating a random password, and checking a passwords complexity level.

Reasoning:
generate_password()

I take advantage of the string module in Python and all the constants it has to offer like string.lowercase/uppercase/punctuation/digit.
This is code that is well tested and it would work much better than me defining the constants myself and using the random module to randomly return characters.
When there is code provided by the language, you should always use it. Why rewrite it when it already exists?
The random.choice method is used to return characters for the length of password provided minus one. I subtracted one becuase if a user wants a password with a number in it; he may or may not get it.
I concatenate that with a randomly generated requirement for the complexity level and return the full string as a randomly generated password.

check_password_level()
When this method is invoked by passing in a random password, the method aims to return a complexity level which is measured according to the characters in the password.
I take advantage of regex to check for all conditions and return an type int which is between 1 & 4 as the password complexity level.
I could have been cool and have not initialized the level to 0 on line 90, but by doing that my code is a little bit more readable. A fellow team mate may ask what that int represents.
The regex module provides the search function which takes the regex to search for along with the string. Again there is already code provided by the language that can be used to determine the complexity of a password.
I avoided looping through each character in the given string to determine if it contains isdigit() or other similar methods.

2) user.py
Reasoning:
A user class has been created to represent a real world popo (plain old Python object). This can also be used to represent the entity that will be 
saved into the sqlite database later on. All nouns should have a model, and respective properties which represnt attributes in a database table.
The default constructor for the user object take the user full name, email, and password.
getters and setters follow for encapsulation purposes.

3) db_user.py

Reasoning:
connect_to_sqlite()
This function returns a connection object to a sqlite client. Functions should do one and only one thing correctly.
I have made this its own function becuase it can be reused. I could have very lazily just connected to the database within the insert_user()
Exception handling is a must with io.

Reasoning:
fetch_user()
This function uses the requests module provided by the language to make an HTTP GET request to the endpoint which returns JSON data that represents a user.
I check to see if the status code is 200 meaning OK, and then parse the JSON from the result.
If not I print out the status code to keep things simple.
The HTTP request can use some exception handling if the status code condition is not used.


Resoning:
insert_user()
This function takes in two paramters namely the user_name and the user_email
I start by connecting to the sqllite instance, and create the table using the raw sql.
Placing raw sql in source code is not a great practice, and an ORM tool should be used. Something similar to LINQ or cx_Oracle.
Then I use my sqllite instance to create the table, insert the data, and select the records for proof.
Exception handling must be used when interacting with the database.

Reasoning:
insert_mass_users()
Part of the test requires that 10 random users be created and inserted into the database.
This is seperate from the orignal insert_user function because that did not require the password to be inserted.
Therefore I have created a seperate function just to tackle the need.
I start out once again by connecting to the database, and opening up a cursor object
A new table is created, and generate 10 user objects using the for loop with the fetch_user() function which returns a user.
Then the data is inserted using the getter functions defined in the user model class.
Exception handling is used to make a transaction with the database once again.
The finally block should always be used when interacting with other resources to perform IO operations.

4) test_password_level.py
This is a unit test class which makes use of the unittest module support provided natively by the Python language.
I create four tests to test all four complexity levels of the password.
A random password is generated, and an assertion is made to test the functions.


Conclusion:
I try to follow the SOLID principles I learned early on.
Classes should do ony and only one thing.
Functions follow the same rule.
Code should be added on easily to the solution without changing to much.
I develop solutions that are well documented, maintained, tested, and make it easy for multiple teams to collaborate.  